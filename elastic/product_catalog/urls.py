# -*- coding: utf-8 -*-

from django.conf.urls import patterns, include, url
from tastypie.api import Api
from api import OfferResource, OfferAdditionalFeatureResource, \
    AccessoryResource, OfferPhotoResource, CatalogPartResource, \
    CatalogItemResource

__author__ = "Igor S. Kovalenko"
__contact__ = "kovalenko@sb-soft.biz"
__site__ = "http://www.elastic-trade-server.org"
__year__ = "2015"
__description__ = "Product catalog package"

v1_api = Api(api_name='v1')


v1_api.register(OfferResource())
v1_api.register(OfferAdditionalFeatureResource())
v1_api.register(AccessoryResource())
v1_api.register(OfferPhotoResource())
v1_api.register(CatalogPartResource())
v1_api.register(CatalogItemResource())


urlpatterns = patterns(
    '',
    url(r'^api/', include(v1_api.urls)),
)
