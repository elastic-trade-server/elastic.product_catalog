# -*- coding: utf-8 -*-

from django.db import models
from django.utils.translation import ugettext_lazy as _
from django.core.exceptions import ObjectDoesNotExist
import mptt
from mptt.models import MPTTModel, TreeForeignKey
from ckeditor.fields import RichTextField
from filer.fields.image import FilerImageField
from elastic.stock.signals import *
from elastic.product.models import Product, Uom
from elastic.product.signals import *
from taggit.managers import TaggableManager


__author__ = "Igor S. Kovalenko"
__contact__ = "kovalenko@sb-soft.biz"
__site__ = "http://www.elastic-trade-server.org"
__year__ = "2015"
__description__ = "Product catalog package"


class OfferManager(models.Manager):
    @staticmethod
    def create_offers_for_goods(sku_list):
        """
        Создать предложения на основе списка SKU товаров
        """
        for sku in sku_list:
            # Ищем товар по складскому коду из накладной
            try:
                product = Product.objects.get(sku=sku)
            except ObjectDoesNotExist:
                # Если товар не найден, то пропускаем SKU
                continue

            if not Offer.objects.filter(product=product).count():  # Если предложений нет вообще, то создаем одно
                Offer.objects.create(
                    name=product.name,
                    slug=product.sku,
                    short_desc=(product.short_desc if product.short_desc else _('No description')),
                    product=product,
                )


class Offer(models.Model):
    """
    Коммерческое предложение
    """

    class Meta:
        verbose_name = _('Offer')
        verbose_name_plural = _('Offers')
        ordering = ["name", ]

    is_active = models.BooleanField(default=True, verbose_name=_('Active'))
    creation_date = models.DateTimeField(auto_now_add=True, verbose_name=_('Creation date'))
    last_modify_date = models.DateTimeField(auto_now=True, verbose_name=_('Last modify date'))
    name = models.CharField(max_length=255, db_index=True, verbose_name=_('Name'))
    slug = models.SlugField(unique=True, verbose_name=_('Slug'))
    short_desc = models.CharField(null=True, blank=True, max_length=255,
                                  verbose_name=_('Short description'))
    description = RichTextField(null=True, blank=True, verbose_name=_('Description'))
    product = models.OneToOneField(Product, verbose_name=_('Product'))
    is_deliverable = models.BooleanField(default=True, verbose_name=_('Is deliverable'))
    is_pre_order = models.BooleanField(default=True, verbose_name=_('Is pre-order'))
    warranty = models.CharField(max_length=255, null=True, blank=True, verbose_name=_('Warranty'))
    related_offers = models.ManyToManyField('self', blank=True, related_name='+',
                                            verbose_name=_('Related offers'))
    accessories = models.ManyToManyField('self', blank=True, through="Accessory", symmetrical=False,
                                         verbose_name=_('Accessories'))
    meta_title = models.CharField(null=True, blank=True, max_length=255)
    meta_keywords = models.CharField(null=True, blank=True, max_length=255)
    meta_description = models.CharField(null=True, blank=True, max_length=255)

    price_cost = models.DecimalField(max_digits=15, decimal_places=2, null=True, blank=True,
                                     verbose_name=_('Price cost'))

    tags = TaggableManager()

    objects = OfferManager()

    def sku(self):
        return self.product.sku if self.product else ''
    sku.short_description = _('SKU')

    def empty_photo(self):
        return {True: _('Yes'), False: _('No')}[bool(self.photos.count())]
    empty_photo.short_description = _('Set photos')

    def empty_desc(self):
        return {True: _('Yes'), False: _('No')}[bool(self.description)]
    empty_desc.short_description = _('Set description')

    def __unicode__(self):
        return u"{0}".format(self.name)


class OfferAdditionalFeature(models.Model):
    """
    Дополнительные характеристики предложения
    """

    class Meta:
        verbose_name = _('Additional feature')
        verbose_name_plural = _('Additional features')

    name = models.CharField(verbose_name=_('Name'), max_length=128)
    uom = models.ForeignKey(Uom, null=True, blank=True, verbose_name=_("Unit of measures"))
    value = models.CharField(verbose_name=_('Value'), max_length=255)
    offer = models.ForeignKey(Offer, related_name='additional_features')

    def __unicode__(self):
        return u"{0}: {1}".format(self.name, self.value)


class Accessory(models.Model):
    """
    Элемент комплекта (аксессуар)
    """

    class Meta:
        verbose_name = _('Accessory')
        verbose_name_plural = _('Accessories')
        ordering = ('order',)

    offer = models.ForeignKey(Offer, verbose_name=_('Offer'))
    extra_offer = models.ForeignKey(Offer, related_name='+', verbose_name=_('Extra offer'))
    order = models.PositiveSmallIntegerField(default=10, verbose_name=_('Order of'))
    discount = models.DecimalField(max_digits=15, decimal_places=2, verbose_name=_('Discount'),
                                   null=True, blank=True, help_text=_('In percentage'))

    def __unicode__(self):
        return unicode(self.offer)


class Photo(models.Model):
    """
    Фотографии товара
    """

    class Meta:
        verbose_name = _('Photo')
        verbose_name_plural = _('Photos')
        ordering = ('order',)

    photo = FilerImageField(null=True, blank=True, related_name='offer_photos', verbose_name=_('Photo'))
    order = models.PositiveSmallIntegerField(default=10, verbose_name=_('Order by'))
    short_desc = models.CharField(null=True, blank=True, max_length=255, verbose_name=_('Short description'))
    offer = models.ForeignKey(Offer, related_name='photos', verbose_name=_('Offer'))


class CatalogItem(models.Model):
    """
    Позиция в товарном каталоге
    """

    class Meta:
        verbose_name = _('Catalog item')
        verbose_name_plural = _('Catalog items')
        ordering = ('order',)

    ITEM_SIZE = (
        ('small', _('Small')),
        ('medium', _('Medium')),
        ('big', _('Big')),
    )

    catalog_part = models.ForeignKey('CatalogPart')
    offer = models.ForeignKey(Offer)
    order = models.PositiveSmallIntegerField(default=10, verbose_name=_('Order of'))
    item_size = models.CharField(max_length=16, choices=ITEM_SIZE, default='small', verbose_name=_('Item size'))


class CatalogPart(MPTTModel):
    """
    Раздел товарного каталога
    """

    class Meta:
        verbose_name = _('Product catalog part')
        verbose_name_plural = _('Product catalog parts')

    class MPTTMeta(object):
        parent_attr = 'parent'

    parent = TreeForeignKey('self', null=True, blank=True, related_name='descendants', verbose_name=_('Parent'))
    name = models.CharField(max_length=255, db_index=True, verbose_name=_('Name'))
    slug = models.SlugField(unique=True, verbose_name=_('Slug'))
    order = models.PositiveSmallIntegerField(default=10)
    code = models.CharField(null=True, blank=True, verbose_name=_('Code'), max_length=128, db_index=True)
    short_desc = models.CharField(null=True, blank=True, max_length=255, verbose_name=_('Short description'))
    description = models.TextField(null=True, blank=True, verbose_name=_('Description'))
    logo = FilerImageField(null=True, blank=True, related_name='catalog_part_logos', verbose_name=_('Logo'))
    meta_title = models.CharField(null=True, blank=True, max_length=255)
    meta_keywords = models.CharField(null=True, blank=True, max_length=255)
    meta_description = models.CharField(null=True, blank=True, max_length=255)
    offers = models.ManyToManyField(Offer, blank=True, through=CatalogItem,
                                    verbose_name=_('Offers'))

    def offers_count(self):
        return self.offers.count()

    offers_count.short_description = _('Offers count')

    def content_type(self):
        if self.offers.filter(is_active=True).count():
            return "PARTITION_WITH_GOODS"
        return "PARTITION_WITHOUT_GOODS"

    def __unicode__(self):
        return self.name


mptt.register(CatalogPart)
