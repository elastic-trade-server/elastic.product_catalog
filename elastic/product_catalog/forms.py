# -*- coding: utf-8 -*-

from django.forms import CharField, IntegerField, DecimalField
from models import *
from ckeditor.widgets import CKEditorWidget
import autocomplete_light
from django.utils.translation import ugettext_lazy as _
import django
from autocomplete_light import forms

__author__ = "Igor S. Kovalenko"
__contact__ = "kovalenko@sb-soft.biz"
__site__ = "http://www.elastic-trade-server.org"
__year__ = "2015"
__description__ = "Product catalog package"


class OfferForm(autocomplete_light.ModelForm):
    class Meta:
        model = Offer
        if django.VERSION >= (1, 6):
            fields = '__all__'
        autocomplete_exclude = ['description', ]

    description = CharField(widget=CKEditorWidget(), required=False, label=_("Description"))


class CatalogPartForm(autocomplete_light.ModelForm):
    class Meta:
        model = CatalogPart
        if django.VERSION >= (1, 6):
            fields = '__all__'
        autocomplete_exclude = ['description', ]

    description = CharField(widget=CKEditorWidget(), required=False, label=_("Description"))


class AccessoryInlineForm(autocomplete_light.ModelForm):
    class Meta:
        model = Accessory
        if django.VERSION >= (1, 6):
            fields = '__all__'
        autocomplete_exclude = ['offer', ]

    order = IntegerField(required=True, initial=10, label=_('Order of'))
    discount = DecimalField(required=False, label=_("Discount"), help_text=_('In percentage'))


class PhotoInlineForm(forms.ModelForm):
    class Meta:
        model = Accessory
        if django.VERSION >= (1, 6):
            fields = '__all__'

    order = IntegerField(required=True, initial=10, label=_('Order of'))
    short_desc = CharField(required=False, label=_("Short description"))


class CatalogItemAdminForm(autocomplete_light.ModelForm):
    class Meta:
        model = CatalogItem
        if django.VERSION >= (1, 6):
            fields = '__all__'
        autocomplete_exclude = ['catalog_part', ]

    order = IntegerField(required=True, initial=10, label=_('Order of'))


class OfferAdditionalFeatureInlineForm(autocomplete_light.ModelForm):
    class Meta:
        model = OfferAdditionalFeature
        if django.VERSION >= (1, 6):
            fields = '__all__'
        autocomplete_exclude = ['offer', ]
