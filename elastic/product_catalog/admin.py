# -*- coding: utf-8 -*-

from django.contrib import admin
from forms import *
from django.utils.translation import ugettext_lazy as _
from mpttadmin.admin import MpttAdmin
from django.contrib.admin.filters import SimpleListFilter
from django.db.models import Q

__author__ = "Igor S. Kovalenko"
__contact__ = "kovalenko@sb-soft.biz"
__site__ = "http://www.elastic-trade-server.org"
__year__ = "2015"
__description__ = "Product catalog package"


class PhotoInline(admin.TabularInline):
    model = Photo
    form = PhotoInlineForm


class AccessoryAdminInline(admin.TabularInline):
    model = Accessory
    extra = 1
    fk_name = 'offer'
    form = AccessoryInlineForm
    sortable_field_name = 'order'
    fields = ['extra_offer', 'order', 'discount', ]


class OfferAdditionalFeatureInline(admin.TabularInline):
    model = OfferAdditionalFeature
    form = OfferAdditionalFeatureInlineForm


class DescFilterSpec(SimpleListFilter):
    title = _('Set description')

    parameter_name = u'description'

    def lookups(self, request, model_admin):
        return (
            ('1', _('Yes'), ),
            ('0', _('No'), ),
        )

    def queryset(self, request, queryset):
        if self.value() == '1':
            return queryset.exclude(Q(description=None) | Q(description=""))
        if self.value() == '0':
            return queryset.filter(Q(description=None) | Q(description=""))
        return queryset


class PhotosFilterSpec(SimpleListFilter):
    title = _('Set photos')

    parameter_name = u'photos'

    def lookups(self, request, model_admin):
        return (
            ('1', _('Yes'), ),
            ('0', _('No'), ),
        )

    def queryset(self, request, queryset):
        kwargs = {}
        if self.value():
            kwargs = {
                '{0}__isnull'.format(self.parameter_name): not bool(int(self.value())),
            }
        if self.value() == '1':
            return queryset.filter(**kwargs)
        if self.value() == '0':
            return queryset.filter(**kwargs)
        return queryset


class OfferAdmin(admin.ModelAdmin):
    list_display = ('slug', 'name', 'creation_date', 'sku', 'empty_photo', 'empty_desc', 'is_active', )
    list_display_links = ('slug', 'name', )
    list_filter = (
        DescFilterSpec,
        PhotosFilterSpec,
        # ('photos__count', admin.RelatedFieldListFilter),
        'is_active',
    )
    form = OfferForm
    prepopulated_fields = {"slug": ("name",)}
    search_fields = ('slug', 'name', 'product__sku', 'product__name',)

    fieldsets = (
        (None, {
            'fields': ['is_active', 'name', 'slug', 'short_desc', 'description', 'price_cost',
                       'tags', ]
        }),
        ('Product', {
            'fields': ['product', 'is_deliverable', 'is_pre_order', 'warranty', ]
        }),
        ('Meta', {
            'classes': ('collapse',),
            'fields': ['meta_title', 'meta_keywords', 'meta_description', ]
        }),
        ('Related', {
            # 'classes': ('collapse',),
            'fields': ['related_offers', ]
        }),
    )

    inlines = [
        OfferAdditionalFeatureInline,
        AccessoryAdminInline,
        PhotoInline,
    ]


admin.site.register(Offer, OfferAdmin)


class CatalogItemAdminInline(admin.TabularInline):
    model = CatalogItem
    extra = 1
    form = CatalogItemAdminForm
    sortable_field_name = 'order'
    fields = ['offer', 'item_size', 'order', ]


class CatalogPartAdmin(MpttAdmin):
    class Meta:
        model = CatalogPart

    tree_title_field = 'name'
    tree_display = ('name', 'offers_count', 'slug', 'code', )
    form = CatalogPartForm
    prepopulated_fields = {"slug": ("name",)}

    fieldsets = (
        (None, {
            'fields':  ['parent', 'name', 'slug', 'order', 'code', 'logo', 'short_desc',
                        'description', ]
        }),
        ('Meta', {
            'classes': ('collapse',),
            'fields': ['meta_title', 'meta_keywords', 'meta_description', ]
        }),
    )
    inlines = [CatalogItemAdminInline, ]

admin.site.register(CatalogPart, CatalogPartAdmin)
