# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models
import filer.fields.image
import mptt.fields
import ckeditor.fields


class Migration(migrations.Migration):

    dependencies = [
        ('filer', '0002_auto_20150606_2003'),
        ('product', '0001_initial'),
    ]

    operations = [
        migrations.CreateModel(
            name='Accessory',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('order', models.PositiveSmallIntegerField(default=10, verbose_name='Order of')),
                ('discount', models.DecimalField(decimal_places=2, max_digits=15, blank=True, help_text='In percentage', null=True, verbose_name='Discount')),
            ],
            options={
                'ordering': ('order',),
                'verbose_name': 'Accessory',
                'verbose_name_plural': 'Accessories',
            },
        ),
        migrations.CreateModel(
            name='CatalogItem',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('order', models.PositiveSmallIntegerField(default=10, verbose_name='Order of')),
                ('item_size', models.CharField(default=b'small', max_length=16, verbose_name='Item size', choices=[(b'small', 'Small'), (b'medium', 'Medium'), (b'big', 'Big')])),
            ],
            options={
                'ordering': ('order',),
                'verbose_name': 'Catalog item',
                'verbose_name_plural': 'Catalog items',
            },
        ),
        migrations.CreateModel(
            name='CatalogPart',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('name', models.CharField(max_length=255, verbose_name='Name', db_index=True)),
                ('slug', models.SlugField(unique=True, verbose_name='Slug')),
                ('order', models.PositiveSmallIntegerField(default=10)),
                ('code', models.CharField(db_index=True, max_length=128, null=True, verbose_name='Code', blank=True)),
                ('short_desc', models.CharField(max_length=255, null=True, verbose_name='Short description', blank=True)),
                ('description', models.TextField(null=True, verbose_name='Description', blank=True)),
                ('meta_title', models.CharField(max_length=255, null=True, blank=True)),
                ('meta_keywords', models.CharField(max_length=255, null=True, blank=True)),
                ('meta_description', models.CharField(max_length=255, null=True, blank=True)),
                ('lft', models.PositiveIntegerField(editable=False, db_index=True)),
                ('rght', models.PositiveIntegerField(editable=False, db_index=True)),
                ('tree_id', models.PositiveIntegerField(editable=False, db_index=True)),
                ('level', models.PositiveIntegerField(editable=False, db_index=True)),
                ('logo', filer.fields.image.FilerImageField(related_name='catalog_part_logos', verbose_name='Logo', blank=True, to='filer.Image', null=True)),
            ],
            options={
                'verbose_name': 'Product catalog part',
                'verbose_name_plural': 'Product catalog parts',
            },
        ),
        migrations.CreateModel(
            name='Offer',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('is_active', models.BooleanField(default=True, verbose_name='Active')),
                ('creation_date', models.DateTimeField(auto_now_add=True, verbose_name='Creation date')),
                ('last_modify_date', models.DateTimeField(auto_now=True, verbose_name='Last modify date')),
                ('name', models.CharField(max_length=255, verbose_name='Name', db_index=True)),
                ('slug', models.SlugField(unique=True, verbose_name='Slug')),
                ('short_desc', models.CharField(max_length=255, null=True, verbose_name='Short description', blank=True)),
                ('description', ckeditor.fields.RichTextField(null=True, verbose_name='Description', blank=True)),
                ('is_deliverable', models.BooleanField(default=True, verbose_name='Is deliverable')),
                ('is_pre_order', models.BooleanField(default=True, verbose_name='Is pre-order')),
                ('warranty', models.CharField(max_length=255, null=True, verbose_name='Warranty', blank=True)),
                ('meta_title', models.CharField(max_length=255, null=True, blank=True)),
                ('meta_keywords', models.CharField(max_length=255, null=True, blank=True)),
                ('meta_description', models.CharField(max_length=255, null=True, blank=True)),
                ('accessories', models.ManyToManyField(to='product_catalog.Offer', verbose_name='Accessories', through='product_catalog.Accessory', blank=True)),
                ('product', models.ForeignKey(verbose_name='Product', blank=True, to='product.Product', null=True)),
                ('related_offers', models.ManyToManyField(related_name='_offer_related_offers_+', verbose_name='Related offers', to='product_catalog.Offer', blank=True)),
            ],
            options={
                'ordering': ['name'],
                'verbose_name': 'Offer',
                'verbose_name_plural': 'Offers',
            },
        ),
        migrations.CreateModel(
            name='OfferAdditionalFeature',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('name', models.CharField(max_length=128, verbose_name='Name')),
                ('value', models.CharField(max_length=255, verbose_name='Value')),
                ('offer', models.ForeignKey(related_name='additional_features', to='product_catalog.Offer')),
                ('uom', models.ForeignKey(verbose_name='Unit of measures', blank=True, to='product.Uom', null=True)),
            ],
            options={
                'verbose_name': 'Additional feature',
                'verbose_name_plural': 'Additional features',
            },
        ),
        migrations.CreateModel(
            name='Photo',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('order', models.PositiveSmallIntegerField(default=10, verbose_name='Order by')),
                ('short_desc', models.CharField(max_length=255, null=True, verbose_name='Short description', blank=True)),
                ('offer', models.ForeignKey(related_name='photos', verbose_name='Offer', to='product_catalog.Offer')),
                ('photo', filer.fields.image.FilerImageField(related_name='offer_photos', verbose_name='Photo', blank=True, to='filer.Image', null=True)),
            ],
            options={
                'ordering': ('order',),
                'verbose_name': 'Photo',
                'verbose_name_plural': 'Photos',
            },
        ),
        migrations.AddField(
            model_name='catalogpart',
            name='offers',
            field=models.ManyToManyField(to='product_catalog.Offer', verbose_name='Offers', through='product_catalog.CatalogItem', blank=True),
        ),
        migrations.AddField(
            model_name='catalogpart',
            name='parent',
            field=mptt.fields.TreeForeignKey(related_name='descendants', verbose_name='Parent', blank=True, to='product_catalog.CatalogPart', null=True),
        ),
        migrations.AddField(
            model_name='catalogitem',
            name='catalog_part',
            field=models.ForeignKey(to='product_catalog.CatalogPart'),
        ),
        migrations.AddField(
            model_name='catalogitem',
            name='offer',
            field=models.ForeignKey(to='product_catalog.Offer'),
        ),
        migrations.AddField(
            model_name='accessory',
            name='extra_offer',
            field=models.ForeignKey(related_name='+', verbose_name='Extra offer', to='product_catalog.Offer'),
        ),
        migrations.AddField(
            model_name='accessory',
            name='offer',
            field=models.ForeignKey(verbose_name='Offer', to='product_catalog.Offer'),
        ),
    ]
