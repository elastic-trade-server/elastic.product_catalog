# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('product_catalog', '0001_initial'),
    ]

    operations = [
        migrations.AlterField(
            model_name='offer',
            name='product',
            field=models.OneToOneField(verbose_name='Product', to='product.Product'),
        ),
    ]
