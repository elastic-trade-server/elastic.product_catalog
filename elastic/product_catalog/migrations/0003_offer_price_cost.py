# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('product_catalog', '0002_auto_20160621_0604'),
    ]

    operations = [
        migrations.AddField(
            model_name='offer',
            name='price_cost',
            field=models.DecimalField(null=True, verbose_name='Price cost', max_digits=15, decimal_places=2, blank=True),
        ),
    ]
