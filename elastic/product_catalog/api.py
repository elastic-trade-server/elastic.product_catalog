# -*- coding: utf-8 -*-


from models import *
from tastypie import fields
from tastypie.resources import ModelResource
from tastypie.authentication import  Authentication
from tastypie.authorization import Authorization
from tastypie.bundle import Bundle
from tastypie.utils.urls import trailing_slash
from tastypie.constants import ALL, ALL_WITH_RELATIONS
from tastypie.utils import dict_strip_unicode_keys
from tastypie import http
from tastypie.exceptions import BadRequest
from django.utils.translation import ugettext_lazy as _
from django.conf.urls import url
from django.core.exceptions import ObjectDoesNotExist
from django.db import transaction
from django.core.files.base import ContentFile
from django.core.urlresolvers import reverse
import base64
from filer.models.imagemodels import Image
from easy_thumbnails.files import get_thumbnailer
from django.http.request import HttpRequest
import json


__author__ = "Igor S. Kovalenko"
__contact__ = "kovalenko@sb-soft.biz"
__site__ = "http://www.elastic-trade-server.org"
__year__ = "2015"
__description__ = "Product catalog package"


class OfferResource(ModelResource):
    class Meta:
        queryset = Offer.objects.filter(is_active=True)
        resource_name = 'offer'
        authentication = Authentication()
        authorization = Authorization()
        excludes = ['id', ]
        filtering = {
            "slug": ALL,
            "product": ALL_WITH_RELATIONS,
        }

    is_active = fields.BooleanField(attribute='is_active', default=True, help_text=_('Active'))
    creation_date = fields.DateTimeField(
        attribute='creation_date', readonly=True, help_text=_('Creation date')
    )
    last_modify_date = fields.DateTimeField(attribute='last_modify_date', readonly=True,
                                            help_text=_('Last modify date'))
    name = fields.CharField(attribute='name', help_text=_('Name'))
    sku = fields.CharField(help_text=_('SKU'))
    slug = fields.CharField(attribute='slug', help_text=_('Slug'))
    short_desc = fields.CharField(
        attribute='short_desc', null=True, blank=True, help_text=_('Short description')
    )
    description = fields.CharField(
        attribute='description', null=True, blank=True, help_text=_('Description')
    )
    product = fields.OneToOneField(
        to="elastic.product.api.ProductResource", attribute='product', null=True, blank=True,
        help_text=_('Product')
    )
    is_deliverable = fields.BooleanField(
        attribute='is_deliverable', default=True, help_text=_('Is deliverable')
    )
    is_pre_order = fields.BooleanField(
        attribute='is_pre_order', default=True, help_text=_('Is pre-order')
    )
    warranty = fields.CharField(
        attribute='warranty', null=True, blank=True, help_text=_('Warranty')
    )
    related_offers = fields.ManyToManyField(
        to='self', attribute='related_offers', null=True, blank=True, help_text=_('Related offers')
    )
    accessories = fields.ToManyField(to='self', attribute='accessories', null=True, blank=True,
                                     readonly=True, help_text=_('Accessories'))
    meta_title = fields.CharField(attribute='meta_title', null=True, blank=True)
    meta_keywords = fields.CharField(attribute='meta_keywords', null=True, blank=True)
    meta_description = fields.CharField(attribute='meta_description', null=True, blank=True)
    photos = fields.ToManyField(
        to='elastic.product_catalog.api.OfferPhotoResource', attribute='photos',
        related_name='offer', full=True, null=True, blank=True, help_text=_('Photos')
    )
    additional_features = fields.ToManyField(
        to='elastic.product_catalog.api.OfferAdditionalFeatureResource',
        attribute='additional_features', null=True, blank=True, full=True,
        related_name='offer', help_text=_('Additional features')
    )
    price_cost = fields.DecimalField(
        attribute='price_cost', null=True, blank=True, help_text=_('Price cost')
    )

    def prepend_urls(self):
        return [
            url(r"^(?P<resource_name>%s)/schema%s$" % (self._meta.resource_name, trailing_slash()),
                self.wrap_view('get_schema'), name="api_get_schema"),
            url(r"^(?P<resource_name>%s)/(?P<sku>[\w\d_.-]+)/$" % self._meta.resource_name,
                self.wrap_view('dispatch_detail'), name="api_dispatch_detail"),
        ]

    def detail_uri_kwargs(self, bundle_or_obj):
        kwargs = {}

        if isinstance(bundle_or_obj, Bundle):
            kwargs['pk'] = bundle_or_obj.obj.product.sku
        else:
            kwargs['pk'] = bundle_or_obj.product.sku

        return kwargs

    def dehydrate(self, bundle):
        bundle.data.pop('is_active')
        return super(OfferResource, self).dehydrate(bundle)

    @staticmethod
    def dehydrate_sku(bundle):
        return bundle.obj.product.sku

    @staticmethod
    def dehydrate_accessories(bundle):
        accessories = []
        for accessory in bundle.obj.accessory_set.iterator():
            accessories.append(
                {
                    'offer': reverse('api_dispatch_detail', kwargs={
                        'api_name': 'v1',  # Or whatever you have set for your api
                        'resource_name': "offer",
                        'pk': accessory.extra_offer.slug
                    }),
                    'discount': accessory.discount,
                    'order': accessory.order
                }
            )
        return accessories

    @staticmethod
    def hydrate_product(bundle):
        if 'product' in bundle.data and type(bundle.data['product']) == dict:
            bundle.data['product'] = reverse('api_dispatch_detail', kwargs={
                'api_name': 'v1',  # Or whatever you have set for your api
                'resource_name': "product/product",
                'pk': Product.objects.get(**bundle.data['product']).sku
            })
        return bundle

    @staticmethod
    def hydrate_related_offers(bundle):
        r = []
        for related_offer in bundle.data.get('related_offers', []):
            if type(related_offer) == dict:
                r.append(reverse('api_dispatch_detail', kwargs={
                    'api_name': 'v1',  # Or whatever you have set for your api
                    'resource_name': "offer",
                    'pk': Offer.objects.get(**related_offer).slug
                }))
        bundle.data['related_offers'] = r
        return bundle

    @staticmethod
    def hydrate_photos(bundle):
        for p in bundle.obj.photos.iterator():
            photo = p.photo
            p.delete()
            if photo.offer_photos.count() == 0:  # Если фотография больше никем не используется...
                photo.delete()  # ... то удаляем ее.
        return bundle

    @staticmethod
    def hydrate_additional_features(bundle):
        # Удаляем дополнительные свойства, - они будут заново определены позднее
        for f in bundle.obj.additional_features.iterator():
            f.delete()
        return bundle

    @transaction.atomic
    def post_list(self, request, **kwargs):
        deserialized = self.deserialize(request, request.body,
                                        format=request.META.get('CONTENT_TYPE', 'application/json'))
        if type(deserialized) is not list:
            deserialized = [deserialized]

        for item in deserialized:
            item = self.alter_deserialized_detail_data(request, item)

            bundle = self.build_bundle(data=dict_strip_unicode_keys(item), request=request)

            try:
                kwargs['pk'] = Offer.objects.get(slug=bundle.data['slug']).pk
                self.obj_update(bundle, False, **self.remove_api_resource_names(kwargs))
            except ObjectDoesNotExist:
                self.obj_create(bundle, **self.remove_api_resource_names(kwargs))

        return http.HttpCreated(location=self.get_resource_uri())

    def obj_get(self, bundle, **kwargs):
        kwargs['pk'] = Offer.objects.get(**{"product__sku": kwargs['sku']}).pk
        return super(OfferResource, self).obj_get(bundle, **kwargs)


class OfferAdditionalFeatureResource(ModelResource):
    class Meta:
        queryset = OfferAdditionalFeature.objects.all()
        resource_name = 'offer_additional_feature'
        authentication = Authentication()
        authorization = Authorization()
        include_resource_uri = False
        excludes = ['id', ]

    uom = fields.ToOneField(to="elastic.product.api.UomResource", full=True, null=True, blank=True, attribute='uom',
                            help_text=_('Uom'))
    name = fields.CharField(attribute='name', help_text=_('Name'))
    value = fields.CharField(attribute='value', help_text=_('Value'))
    offer = fields.ToOneField(to=OfferResource, full=False, attribute='offer', help_text=_('Offer'))

    @staticmethod
    def hydrate_uom(bundle):
        if 'uom' in bundle.data and type(bundle.data['uom']) == dict:
            bundle.data['uom'] = reverse('api_dispatch_detail', kwargs={
                'api_name': 'v1',  # Or whatever you have set for your api
                'resource_name': "product/uom",
                'pk': Uom.objects.get(**bundle.data['uom']).code
            })
        return bundle

    def dehydrate(self, bundle):
        if 'uom' in bundle.data:
            bundle.data.pop('uom')

        uom = bundle.obj.uom

        if uom:
            bundle.data['uom'] = uom.code

        bundle.data.pop('offer')

        return bundle


class AccessoryResource(ModelResource):
    class Meta:
        queryset = Accessory.objects.all()
        resource_name = 'offer_accessory'
        authentication = Authentication()
        authorization = Authorization()
        excludes = ['id', ]
        filtering = {
            "offer": ALL_WITH_RELATIONS,
            "extra_offer": ALL_WITH_RELATIONS,
        }

    offer = fields.ToOneField(to=OfferResource, full=False, attribute='offer', help_text=_('Offer'))
    extra_offer = fields.ToOneField(to=OfferResource, full=False, attribute='extra_offer', help_text=_('Offer'))
    order = fields.IntegerField(attribute='order', default=10, help_text=_('Order of'))
    discount = fields.DecimalField(attribute='discount', null=True, blank=True, help_text=_('Discount'))

    @staticmethod
    def hydrate_offer(bundle):
        if type(bundle.data['offer']) == dict:
            bundle.data['offer'] = reverse('api_dispatch_detail', kwargs={
                'api_name': 'v1',  # Or whatever you have set for your api
                'resource_name': "offer",
                'pk': Offer.objects.get(**bundle.data['offer']).slug
            })
        return bundle

    @staticmethod
    def hydrate_extra_offer(bundle):
        if type(bundle.data['extra_offer']) == dict:
            bundle.data['extra_offer'] = reverse('api_dispatch_detail', kwargs={
                'api_name': 'v1',  # Or whatever you have set for your api
                'resource_name': "offer",
                'pk': Offer.objects.get(**bundle.data['offer']).slug
            })
        return bundle


class OfferPhotoResource(ModelResource):
    class Meta:
        queryset = Photo.objects.all()
        resource_name = 'offer_photo'
        authentication = Authentication()
        authorization = Authorization()
        include_resource_uri = False
        excludes = ('id', )

    photo = fields.CharField(attribute='photo', help_text=_('Photo url'))
    photo_filename = fields.CharField(help_text=_('Photo file name'))
    order = fields.IntegerField(attribute='order', default=10, help_text=_('Order of'))
    short_desc = fields.CharField(attribute='short_desc', null=True, blank=True, help_text=_('Short description'))
    offer = fields.ToOneField(to=OfferResource, full=False, attribute='offer', help_text=_('Offer'))

    def dehydrate(self, bundle):
        try:
            op = {
                'size': (
                    int(bundle.request.GET.get('width')),
                    int(bundle.request.GET.get('height'))),
                'crop': bundle.request.GET.get('crop', True)
            }
        except KeyError:
            bundle.data['photo'] = bundle.obj.photo.url
        except TypeError:
            bundle.data['photo'] = bundle.obj.photo.url
        else:
            bundle.data['photo'] = get_thumbnailer(bundle.obj.photo).get_thumbnail(op).url

        bundle.data.pop('photo_filename')
        bundle.data.pop('offer')

        print bundle.request.GET

        return bundle

    def hydrate(self, bundle):
        # Создаем изображение на базе контента изображение
        img = Image()
        img.file = ContentFile(base64.decodestring(bundle.data['photo']), name=bundle.data['photo_filename'])
        img.original_filename = bundle.data['photo_filename']
        img.save()

        # Запихиваем изображение в поле
        bundle.obj.photo = img

        # Подчищаем за собой
        del bundle.data['photo']
        del bundle.data['photo_filename']

        return super(OfferPhotoResource, self).hydrate(bundle)


class CatalogItemResource(ModelResource):
    class Meta:
        queryset = CatalogItem.objects.all()
        resource_name = 'catalog_item'
        authentication = Authentication()
        authorization = Authorization()
        filtering = {
            "offer": ALL_WITH_RELATIONS,
            "catalog_part": ALL_WITH_RELATIONS,
        }

    catalog_part = fields.ToOneField(to="elastic.product_catalog.api.CatalogPartResource", full=False,
                                     attribute='catalog_part', help_text=_('Catalog part'))
    offer = fields.ToOneField(to=OfferResource, full=True, attribute='offer', help_text=_('Offer'))
    order = fields.IntegerField(attribute='order', default=10, help_text=_('Order of'))
    item_size = fields.CharField(attribute='item_size', default='small', help_text=_('Display item size'))

    @staticmethod
    def hydrate_catalog_part(bundle):
        if 'catalog_part' in bundle.data and type(bundle.data['catalog_part']) == dict:
            bundle.data['catalog_part'] = reverse('api_dispatch_detail', kwargs={
                'api_name': 'v1',  # Or whatever you have set for your api
                'resource_name': "catalog_part",
                'pk': CatalogPart.objects.get(**bundle.data['catalog_part']).slug
            })

        return bundle

    @staticmethod
    def hydrate_offer(bundle):
        if type(bundle.data['offer']) == dict:
            bundle.data['offer'] = reverse('api_dispatch_detail', kwargs={
                'api_name': 'v1',  # Or whatever you have set for your api
                'resource_name': "offer",
                'pk': Offer.objects.get(**bundle.data['offer']).slug
            })
        return bundle

    def dehydrate_offer(self, bundle):
        offer_res = self.offer.to_class()
        offer_request = HttpRequest()
        offer_request.user = getattr(bundle.request, 'user', None)
        offer_request.session = getattr(bundle.request, 'session', None)

        try:
            offer_request.GET['width'] = int(bundle.request.GET.get('{0}_width'.format(bundle.obj.item_size)))
            offer_request.GET['height'] = int(bundle.request.GET.get('{0}_height'.format(bundle.obj.item_size)))
            offer_request.GET['crop'] = bundle.request.GET.get('crop', True)
        except KeyError:
            pass
        except TypeError:
            pass

        offer_bundle = offer_res.build_bundle(request=offer_request, obj=bundle.obj.offer)

        return json.loads(offer_res.serialize(None, offer_res.full_dehydrate(offer_bundle), "application/json"))

    @transaction.atomic
    def post_list(self, request, **kwargs):
        deserialized = self.deserialize(request, request.body,
                                        format=request.META.get('CONTENT_TYPE', 'application/json'))
        if type(deserialized) is not list:
            deserialized = [deserialized]

        for item in deserialized:
            item = self.alter_deserialized_detail_data(request, item)

            bundle = self.build_bundle(data=dict_strip_unicode_keys(item), request=request)

            try:
                kwargs['pk'] = CatalogItem.objects.get(pk=bundle.data.get('id', None)).pk
                self.obj_update(bundle, False, **self.remove_api_resource_names(kwargs))
            except ObjectDoesNotExist:
                self.obj_create(bundle, **self.remove_api_resource_names(kwargs))
            except Exception as e:
                return http.HttpBadRequest(str(e))

        return http.HttpCreated(location=self.get_resource_uri())


class CatalogPartResource(ModelResource):
    class Meta:
        queryset = CatalogPart.objects.exclude(slug='unsorted')
        resource_name = 'catalog_part'
        authentication = Authentication()
        authorization = Authorization()
        excludes = ['id', 'tree_id', 'rght', 'lft', 'level', ]
        filtering = {
            "slug": ALL,
            "parent": ALL_WITH_RELATIONS,
            "offers": ALL_WITH_RELATIONS,
            'descendants': ALL_WITH_RELATIONS,
        }

    parent = fields.ToOneField(to='self', attribute='parent', null=True, blank=True, full=False, help_text=_('Parent'))
    name = fields.CharField(attribute='name', help_text=_('Name'))
    slug = fields.CharField(attribute='slug', help_text=_('Slug'))
    order = fields.IntegerField(attribute='order', default=10, help_text=_('Order of'))
    code = fields.CharField(attribute='code', null=True, blank=True, help_text=_('Code'))
    short_desc = fields.CharField(attribute='short_desc', null=True, blank=True, help_text=_('Short description'))
    description = fields.CharField(attribute='description', null=True, blank=True, help_text=_('Description'))

    logo = fields.CharField(attribute='logo', null=True, blank=True, help_text=_('Logo url'))
    logo_filename = fields.CharField(help_text=_('Logo file name'))

    meta_title = fields.CharField(attribute='meta_title', null=True, blank=True)
    meta_keywords = fields.CharField(attribute='meta_keywords', null=True, blank=True)
    meta_description = fields.CharField(attribute='meta_description', null=True, blank=True)
    items = fields.ManyToManyField(to=CatalogItemResource, attribute='catalogitem_set', full=False,
                                   null=True, blank=True, help_text=_('Items'))
    descendants = fields.ManyToManyField(to='self', attribute='descendants', null=True, blank=True, full=True,
                                         help_text=_('Descendants'))

    def prepend_urls(self):
        return [
            url(r"^(?P<resource_name>%s)/schema%s$" % (self._meta.resource_name, trailing_slash()),
                self.wrap_view('get_schema'), name="api_get_schema"),
            url(r"^(?P<resource_name>%s)/(?P<slug>[\w\d_.-]+)/$" % self._meta.resource_name,
                self.wrap_view('dispatch_detail'), name="api_dispatch_detail"),
        ]

    def detail_uri_kwargs(self, bundle_or_obj):
        kwargs = {}

        if isinstance(bundle_or_obj, Bundle):
            kwargs['pk'] = bundle_or_obj.obj.slug
        else:
            kwargs['pk'] = bundle_or_obj.slug

        return kwargs

    def dehydrate(self, bundle):
        bundle.data['logo'] = bundle.obj.logo.url if bundle.obj.logo else None
        del bundle.data['logo_filename']
        return bundle

    # @staticmethod
    # def dehydrate_items(bundle):
    #     catalog_items = []
    #     for item in bundle.obj.catalogitem_set.iterator():
    #         catalog_items.append(
    #             {
    #                 'offer': OfferResource().full_dehydrate(Bundle(obj=item.offer)),
    #                 'item_size': item.item_size,
    #                 'order': item.order
    #             }
    #         )
    #     return catalog_items

    @staticmethod
    def hydrate_logo(bundle):
        # Создаем изображение на базе контента изображение
        if 'logo' in bundle.data and bundle.data['logo'] and bundle.data['logo_filename']:
            img = Image()
            img.file = ContentFile(base64.decodestring(bundle.data['logo']), name=bundle.data['logo_filename'])
            img.original_filename = bundle.data['logo_filename']
            img.save()

            # Запихиваем изображение в поле
            bundle.obj.logo = img
            bundle.data['logo'] = bundle.obj.logo

            # Подчищаем за собой
            del bundle.data['logo_filename']

        return bundle

    @staticmethod
    def hydrate_parent(bundle):
        if type(bundle.data.get('parent')) == unicode or type(bundle.data.get('parent')) == str:
            try:
                bundle.obj.parent = CatalogPart.objects.get(slug=bundle.data['parent'])
                bundle.data.pop('parent')
            except ObjectDoesNotExist as e:
                raise BadRequest("{0}, code={1}, parent code={2}".format(e, bundle.data['slug'],
                                                                         bundle.data['parent']))
        if type(bundle.data.get('parent')) == dict:
            try:
                bundle.obj.parent = CatalogPart.objects.get(slug=bundle.data['parent']['slug'])
                bundle.data.pop('parent')
            except ObjectDoesNotExist as e:
                raise BadRequest("{0}, code={1}, parent code={2}".format(e, bundle.data['slug'],
                                                                         bundle.data['parent']['slug']))
        return bundle

    @transaction.atomic
    def post_list(self, request, **kwargs):
        deserialized = self.deserialize(request, request.body,
                                        format=request.META.get('CONTENT_TYPE', 'application/json'))
        if type(deserialized) is not list:
            deserialized = [deserialized]

        for item in deserialized:
            item = self.alter_deserialized_detail_data(request, item)

            bundle = self.build_bundle(data=dict_strip_unicode_keys(item), request=request)

            try:
                part = CatalogPart.objects.get(slug=bundle.data['slug'])
                kwargs['pk'] = part.pk

                # Если старая картинка используется только в этом разделе
                if 'logo' in bundle.data and part.logo and part.logo.image.catalog_part_logos.count() == 1:
                    part.logo.image.delete()  # ... то удаляем ее.
                part.logo = None
                part.save()

                try:
                    self.obj_update(bundle, False, **self.remove_api_resource_names(kwargs))
                except Exception as e:
                    return http.HttpBadRequest(str(e))
            except ObjectDoesNotExist:
                self.obj_create(bundle, **self.remove_api_resource_names(kwargs))
            except Exception as e:
                return http.HttpBadRequest(str(e))

        return http.HttpCreated(location=self.get_resource_uri())
